public class StudentRecord{
	public String name;
	private static int studentCount;
	
	public String getName(){
		return name;
	}

	public void setName(String temp){
		name = temp;
	}
	
	public static int getStudentCount(){
		return studentCount;
	}
	
	public StudentRecord (){
		//beberapa kode inisialisasi di sini
	}
	
	public StudentRecord (String temp){
		this.name = temp;
	}
}
