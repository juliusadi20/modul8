public class StudentRecordExample2 {
	public static void main (String[] args){
		//membuat 3 object StudentRecord
		StudentRecord2 annaRecord = new StudentRecord2();
		StudentRecord2 beahRecord = new StudentRecord2();
		StudentRecord2 crisRecord = new StudentRecord2();
		
		//versi baru yang ditambahkan 
		StudentRecord2 karyono = new StudentRecord2();
		StudentRecord2 singjongki = new StudentRecord2();
		StudentRecord2 masbejo = new StudentRecord2();
		
		//memberi nama siswa 
		annaRecord.setName("Anna");
		beahRecord.setName("Beah");
		crisRecord.setName("Cris");
		
		//menampilkan nama siswa "Anna"
		System.out.println(annaRecord.getName());
		
		//menampilkan 2 nama lainnya
		System.out.println(beahRecord.getName());
		System.out.println(crisRecord.getName());
		
		//menampilkan jumlah siswa 
		System.out.println("Count="+StudentRecord2.getStudentRecord());
		StudentRecord2 anna2Record = new StudentRecord2();
		anna2Record.setName("Anna");
		anna2Record.setAddress("Philipina");
		anna2Record.setAge(15);
		anna2Record.setMathGrade(80);
		anna2Record.setEnglishGrade(95.5);
		anna2Record.setScienceGrade(100);

		//overload method
		anna2Record.print(anna2Record.getName());
		anna2Record.print(anna2Record.getEnglishGrade(),
		anna2Record.getMathGrade(),
		anna2Record.getScienceGrade());
		anna2Record.print(anna2Record.getName());
	}
}