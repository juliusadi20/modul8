public class PublicElevatorTest{
	public static void main(String args[]){
		PublicElevator pubElevator = new PublicElevator ();
		pubElevator.bukaPintu = true; //Penumpang Masuk
		pubElevator.bukaPintu = false; //Pintu Tutup
		//Pergi ke lantai 0 dibawah gedung 

		pubElevator.lantaiSkrg--;
		pubElevator.lantaiSkrg++;
		
		//Lompat ke lantai 7 (Hanya ada 5 lantai dalam gedung
		pubElevator.lantaiSkrg = 7;
		pubElevator.bukaPintu = true; //penumpang masuk/keluar
		pubElevator.bukaPintu = false;
		pubElevator.lantaiSkrg = 1;//menuju lantai pertama
		pubElevator.bukaPintu = true; //penumpang masuk/keluar
		pubElevator.lantaiSkrg++; //Elevator bergerak tanpa menutup pintu
		pubElevator.bukaPintu=false;
		pubElevator.lantaiSkrg--;
		pubElevator.lantaiSkrg--;
	}
}