public class PrivateShirt2{
	private int idBaju = 0; //ID Default untuk baju
	private String keterangan = "-Keterangan Diperlukan-"; //default
	
	//Kode Warna R=Merah, G=Hijau, B=Biru, U=Tidak Ditentukan
	private char kodeWarna = 'U';
	private double haraga = 0.0; //harga default
	private int jmlStok = 0; //Default untuk jumlah barang
	public char getKodeWarna (){
		return kodeWarna;
	}
	public void setKodeWarna (char kode){
		switch (kode){
			case 'R':
			case 'G':
			case 'B':
			kodeWarna = kode;
			break;
			default:
			System.out.println("kodeWarna salah, gunakan R, G, atau B");
		}
	}
	
	public int getidBaju(){
		return idBaju;
	}
	public void setidBaju(int Id){
		idBaju = Id;
	}

	public String getKeterangan(){
		return keterangan;
	}
	public void setKeterangan(String abc){
		keterangan = abc;
	}
	
}