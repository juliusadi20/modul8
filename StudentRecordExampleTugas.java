public class StudentRecordExampleTugas {
	public static void main (String[] args){
	//membuat 3 object StudentRecord
	StudentRecordTugas annaRecord = new StudentRecordTugas();
	StudentRecordTugas beahRecord = new StudentRecordTugas();
	StudentRecordTugas crisRecord = new StudentRecordTugas();
	
	//memberi nama siswa 
	annaRecord.setName("Anna");
	
	//lengkapi untuk nama 2 siswa lainnya!
	beahRecord.setName("Beah");
	crisRecord.setName("Cris");

	//menampilkan nama siswa "Anna"
	System.out.println( annaRecord.getName() );
	//lengkapi untuk 2 nama lainnya
	System.out.println( beahRecord.getName() );
	System.out.println( crisRecord.getName() );

	//menampilkan jumlah siswa 
	System.out.println("Count="+StudentRecordTugas.getStudentCount());
	}
}