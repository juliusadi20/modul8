public class StudentRecordTugas {
	private String name;
	private String address;
	private int age;
	private double mathGrade;
	private double englishGrade;
	private double scienceGrade;
 	private double average;
	private static int STUDENTCOUNT;	
	
	public String getName(){
		return name;
	}
	
	public void setName(String temp){
		name = temp;
	}
	
	public double getAverage(){
		double result = 0;
		result = (mathGrade+englishGrade+scienceGrade)/3;
		return result;
	}
	
	public static int getStudentCount(){
		return STUDENTCOUNT;
	}
	
	public StudentRecordTugas (){ //costructor default 
		STUDENTCOUNT++;
	}
	
	public StudentRecordTugas(String temp){
		this.name = temp;
		STUDENTCOUNT++;
	}
	
	public StudentRecordTugas(String name, String Address){
		this.name = name;
		this.address = Address;
		STUDENTCOUNT++;
	}
	
	public StudentRecordTugas(double mGrade, double eGrade, double sGrade){
		mathGrade = mGrade;
		englishGrade = eGrade;
		scienceGrade = sGrade;
		STUDENTCOUNT++;
	}
}